FROM python:3.7-slim

ARG SSH_PRIVATE_KEY

RUN mkdir -p ~/.config/matplotlib
RUN echo 'backend: Agg' > ~/.config/matplotlib/matplotlibrc
RUN apt update && \
    apt install -y tk tcl zip && \
    apt install -y git && \
    apt install -y curl

RUN mkdir /workspace
WORKDIR /workspace

ADD requirements.txt /workspace
RUN pip install --trusted-host pypi.python.org -r requirements.txt && \
    pip install matplotlib && \
    pip install pandas && \
    pip install html5lib && \
    pip install numpy

# force cache invalidation if needed
ARG COMMONS_PYTHON_SHASUM
RUN echo $COMMONS_PYTHON_SHASUM
# install commons-python
RUN pip install -e git+https://gitlab.com/openfeignasses/commons-python.git#egg=cryptobot_commons

ADD . .

RUN chmod +x entrypoint.sh
RUN chmod +x wait-for-it.sh

ENTRYPOINT [ "./entrypoint.sh" ]

CMD cd src/main && \
    python execute.py