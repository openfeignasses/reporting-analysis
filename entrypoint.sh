#!/bin/bash

set -e

# Start in watching mode for production
if [ -n "$KEEP_WATCHING_ALLOCATIONS_RESULTS" ]; then
    echo "Starting with keep watching allocations_results table mode ..."
    while [ true ]
    do
        python src/main/should_run.py && \
        python src/main/execute.py && \
        curl --header "Content-Type: multipart/form-data" \
             --request POST -F file=@src/outputs/reporting-analysis.html \
             https://discord.com/api/webhooks/$WEBHOOK_ID/$WEBHOOK_TOKEN && \
        sleep 480 # sleep an additional 8 minutes if executed.

        sleep 120 # loop every 2 minutes anyway
    done
fi

# By default start with what is defined in CMD
exec "$@"