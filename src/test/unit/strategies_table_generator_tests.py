import unittest
import datetime
import pandas as pd
import pandas.testing as pd_testing
import numpy as np
from cryptobot_commons import InvestmentUniverse
from cryptobot_commons import Signal
from cryptobot_commons import StrategiesResults
from strategies_table_generator import StrategiesTableGenerator


def generate_a_strategies_results(execution_id:int, currency_and_position:dict, date:int):
    strategies_results_raw = []
    for currency in list(currency_and_position.keys()):
        strategies_results_raw.append({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse[currency],
            "signal": Signal[currency_and_position[currency]],
            "add_date": datetime.datetime.fromtimestamp(date),
            "execution_id": execution_id
        })
    return StrategiesResults(strategies_results_raw)


class StrategiesTableGeneratorTestCase(unittest.TestCase):

    def setUp(self):
        self.list_strategies_results = []

        self.list_strategies_results.append(
            generate_a_strategies_results(execution_id=1, currency_and_position={"BTC_EUR":"LONG", "LTC_EUR":"LONG", "ETH_EUR":"LONG"}, date=1600534800)
        )

        self.list_strategies_results.append(
            generate_a_strategies_results(execution_id=2, currency_and_position={"BTC_EUR":"SHORT", "LTC_EUR":"LONG", "ETH_EUR":"LONG"}, date=1600448400)
        )

        self.list_strategies_results.append(
            generate_a_strategies_results(execution_id=3, currency_and_position={"BTC_EUR":"SHORT", "LTC_EUR":"NEUTRAL", "ETH_EUR":"LONG"}, date=1600362000)
        )

    def test_instanciate_class(self):
        StrategiesTableGenerator(self.list_strategies_results)

    def test_distinct_pairs_is_correct(self):
        strategies_table_generator = StrategiesTableGenerator(self.list_strategies_results)
        assert len(strategies_table_generator.distinct_pairs) == 3
        assert "BTC_EUR" in strategies_table_generator.distinct_pairs
        assert "LTC_EUR" in strategies_table_generator.distinct_pairs
        assert "ETH_EUR" in strategies_table_generator.distinct_pairs

    def test_distinct_execution_id_is_correct(self):
        strategies_table_generator = StrategiesTableGenerator(self.list_strategies_results)
        assert len(strategies_table_generator.distinct_pairs) == 3
        assert 1 in strategies_table_generator.distinct_execution_id
        assert 2 in strategies_table_generator.distinct_execution_id
        assert 3 in strategies_table_generator.distinct_execution_id

    def test_instanciate_class_wrong_param1(self):
        with self.assertRaises(Exception):
            StrategiesTableGenerator({"BTC_EUR":"LONG", "LTC_EUR":"LONG", "ETH_EUR":"LONG"})

    def test_instanciate_class_wrong_param2(self):
        with self.assertRaises(Exception):
            StrategiesTableGenerator([{"BTC_EUR":"LONG", "LTC_EUR":"LONG", "ETH_EUR":"LONG"}])
    
    def test_execute(self):
        df_we_want = pd.DataFrame(index=[1,2,3], columns=["BTC_EUR", "LTC_EUR", "ETH_EUR"])
        df_we_want["BTC_EUR"] = ["LONG", "SHORT", "SHORT"]
        df_we_want["LTC_EUR"] = ["LONG", "LONG", "NEUTRAL"]
        df_we_want["ETH_EUR"] = ["LONG", "LONG", "LONG"]
        df_we_want = df_we_want.reindex(sorted(df_we_want.columns), axis=1)
        df_we_want = df_we_want.sort_index()
        index_we_want = [1600534800, 1600448400, 1600362000]
        index_we_want = [datetime.datetime.fromtimestamp(element) for element in index_we_want]
        df_we_want.index = index_we_want
        df_strategies_table_generator = StrategiesTableGenerator(self.list_strategies_results).execute()
        pd_testing.assert_frame_equal(df_we_want, df_strategies_table_generator)
