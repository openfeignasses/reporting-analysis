import unittest
from datetime import datetime
import pandas as pd
import pandas.testing as pd_testing
from cryptobot_commons import AllocationResults
from cryptobot_commons import InvestmentUniverse
from cryptobot_commons import Signal
from cryptobot_commons import StrategiesResults
from utils import Utils


class UtilsTestCase(unittest.TestCase):
    def test_sort_success1(self):
        strategies_results = StrategiesResults([{
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.now(),
            "execution_id": 1
        }, {
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.now(),
            "execution_id": 2
        }])
        strategies_results_sorted = Utils.sort_strategies_by_execution_id(strategies_results)
        self.assertTrue(isinstance(strategies_results_sorted, list))
        self.assertTrue(len(strategies_results_sorted) == 2)
        self.assertTrue(isinstance(strategies_results_sorted[0], StrategiesResults))
        self.assertTrue(isinstance(strategies_results_sorted[1], StrategiesResults))

    def test_sort_success2(self):
        strategies_results = StrategiesResults([{
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.now(),
            "execution_id": 2
        }, {
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.now(),
            "execution_id": 1
        }])
        strategies_results_sorted = Utils.sort_strategies_by_execution_id(strategies_results)
        self.assertTrue(isinstance(strategies_results_sorted, list))
        self.assertTrue(len(strategies_results_sorted) == 2)
        self.assertTrue(strategies_results_sorted[0].results[0].execution_id == 1)
        self.assertTrue(strategies_results_sorted[1].results[0].execution_id == 2)

    def test_sort_success3(self):
        strategies_results = StrategiesResults([{
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.now(),
            "execution_id": 2
        }, {
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.now(),
            "execution_id": 1
        }, {
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.now(),
            "execution_id": 2
        }])
        strategies_results_sorted = Utils.sort_strategies_by_execution_id(strategies_results)
        self.assertTrue(isinstance(strategies_results_sorted, list))
        self.assertTrue(len(strategies_results_sorted) == 2)
        self.assertTrue(strategies_results_sorted[0].results[0].execution_id == 1)
        self.assertTrue(strategies_results_sorted[1].results[0].execution_id == 2)
        self.assertTrue(strategies_results_sorted[1].results[1].execution_id == 2)

    def test_sort_fail1(self):
        with self.assertRaises(Exception):
            strategies_results = StrategiesResults([])
            Utils.sort_strategies_by_execution_id(strategies_results)

    def test_sort_fail2(self):
        with self.assertRaises(Exception):
            Utils.sort_strategies_by_execution_id(None)

    def test_sort_success_with_allocations_results(self):
        allocations_results = AllocationResults([{
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 1,
            "value_percent": 0.5,
            "add_date": datetime.now()
        }, {
            "currency": "LTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 2,
            "value_percent": 0.25,
            "add_date": None
        }])
        allocation_results_sorted = Utils.sort_allocation_by_execution_id(allocations_results)
        self.assertTrue(isinstance(allocation_results_sorted, list))
        self.assertTrue(len(allocation_results_sorted) == 2)
        self.assertTrue(isinstance(allocation_results_sorted[0], AllocationResults))
        self.assertTrue(isinstance(allocation_results_sorted[1], AllocationResults))

    def test_merge_strategy_tables(self):
        index_we_want = [1588619100, 1588637100, 1588655100]
        index_we_want = [datetime.fromtimestamp(element) for element in index_we_want]

        strategy_table = pd.DataFrame(index=[1,2,3], columns=["BTC_EUR", "LTC_EUR", "ETH_EUR"])
        strategy_table["BTC_EUR"] = ["LONG", "SHORT", "SHORT"]
        strategy_table["LTC_EUR"] = ["LONG", "LONG", "NEUTRAL"]
        strategy_table["ETH_EUR"] = ["LONG", "LONG", "LONG"]
        strategy_table = strategy_table.reindex(sorted(strategy_table.columns), axis=1)
        strategy_table = strategy_table.sort_index()
        strategy_table.index = index_we_want

        strategy_evaluation_table = pd.DataFrame(index=[1,2,3], columns=["BTC_EUR", "LTC_EUR", "ETH_EUR"])
        strategy_evaluation_table["BTC_EUR"] = ["True", "False", "False"]
        strategy_evaluation_table["LTC_EUR"] = ["True", "False", "False"]
        strategy_evaluation_table["ETH_EUR"] = ["True", "False", "False"]
        strategy_evaluation_table = strategy_evaluation_table.reindex(sorted(strategy_evaluation_table.columns), axis=1)
        strategy_evaluation_table = strategy_evaluation_table.sort_index()
        strategy_evaluation_table.index = index_we_want

        df_we_want = pd.DataFrame(index=[1,2,3], columns=["BTC_EUR", "LTC_EUR", "ETH_EUR"])
        df_we_want["BTC_EUR"] = ["LONG " + Utils.INDICATOR_OK, "SHORT", "SHORT"]
        df_we_want["LTC_EUR"] = ["LONG " + Utils.INDICATOR_OK, "LONG", "NEUTRAL"]
        df_we_want["ETH_EUR"] = ["LONG " + Utils.INDICATOR_OK, "LONG", "LONG"]
        df_we_want = df_we_want.reindex(sorted(df_we_want.columns), axis=1)
        df_we_want = df_we_want.sort_index()
        df_we_want.index = index_we_want

        strategy_table_final = Utils.merge_strategy_tables(strategy_table, strategy_evaluation_table)
        pd_testing.assert_frame_equal(df_we_want, strategy_table_final)

    def test_get_last_values_of_a_df(self):
        index_initial = [1588619100, 1588637100, 1588655100]
        index_initial = [datetime.fromtimestamp(element) for element in index_initial]
        df_initial = pd.DataFrame(index=[1,2,3], columns=["BTC_EUR", "LTC_EUR", "ETH_EUR"])
        df_initial["BTC_EUR"] = ["LONG:T", "SHORT:F", "SHORT:F"]
        df_initial["LTC_EUR"] = ["LONG:T", "LONG:F", "NEUTRAL:F"]
        df_initial["ETH_EUR"] = ["LONG:T", "LONG:F", "LONG:F"]
        df_initial = df_initial.reindex(sorted(df_initial.columns), axis=1)
        df_initial = df_initial.sort_index()
        df_initial.index = index_initial

        index_we_want = [1588637100, 1588655100]
        index_we_want = [datetime.fromtimestamp(element) for element in index_we_want]

        df_we_want = pd.DataFrame(index=[1,2], columns=["BTC_EUR", "LTC_EUR", "ETH_EUR"])
        df_we_want["BTC_EUR"] = ["SHORT:F", "SHORT:F"]
        df_we_want["LTC_EUR"] = ["LONG:F", "NEUTRAL:F"]
        df_we_want["ETH_EUR"] = ["LONG:F", "LONG:F"]
        df_we_want = df_we_want.reindex(sorted(df_we_want.columns), axis=1)
        df_we_want = df_we_want.sort_index()
        df_we_want.index = index_we_want
        new_df = Utils.get_last_values_of_a_df(df_initial, 2)

        pd_testing.assert_frame_equal(df_we_want, new_df)


if __name__ == '__main__':
    unittest.main()
