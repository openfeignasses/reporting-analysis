import unittest
import datetime
import pandas as pd
import pandas.testing as pd_testing
import numpy as np
from cryptobot_commons import InvestmentUniverse
from cryptobot_commons import AllocationResults
from allocations_table_generator import AllocationsTableGenerator


def generate_an_allocation_results(execution_id:int, currency_and_allocation:dict, date:int):
    allocation_results_raw = []
    for currency in list(currency_and_allocation.keys()):
        allocation_results_raw.append({
            "currency": currency[0:3],
            "allocation_name": "Equirepartition",
            "execution_id": execution_id,
            "value_percent": currency_and_allocation[currency],
            "add_date": datetime.datetime.fromtimestamp(date)
        })
    return AllocationResults(allocation_results_raw)


class AllocationTableGeneratorTestCase(unittest.TestCase):

    def setUp(self):
        self.list_allocations_results = []

        self.list_allocations_results.append(
            generate_an_allocation_results(execution_id=1, currency_and_allocation={"BTC":float(0.4), "LTC":float(0.2), "ETH":float(0.4)}, date=1600534800)
        )

        self.list_allocations_results.append(
            generate_an_allocation_results(execution_id=2, currency_and_allocation={"BTC":float(0.5), "LTC":float(0.5), "ETH":float(0.5)}, date=1600448400)
        )

        self.list_allocations_results.append(
            generate_an_allocation_results(execution_id=3, currency_and_allocation={"BTC":float(0.5), "LTC":float(0.2), "ETH":float(0.3)}, date=1600362000)
        )

    def test_instanciate_class(self):
        AllocationsTableGenerator(self.list_allocations_results)

    def test_distinct_pairs_is_correct(self):
        allocations_table_generator = AllocationsTableGenerator(self.list_allocations_results)
        assert len(allocations_table_generator.distinct_pairs) == 3
        assert "BTC" in allocations_table_generator.distinct_pairs
        assert "LTC" in allocations_table_generator.distinct_pairs
        assert "ETH" in allocations_table_generator.distinct_pairs

    def test_distinct_execution_id_is_correct(self):
        allocations_table_generator = AllocationsTableGenerator(self.list_allocations_results)
        assert len(allocations_table_generator.distinct_pairs) == 3
        assert 1 in allocations_table_generator.distinct_execution_id
        assert 2 in allocations_table_generator.distinct_execution_id
        assert 3 in allocations_table_generator.distinct_execution_id

    def test_instanciate_class_wrong_param1(self):
        with self.assertRaises(Exception):
            AllocationsTableGenerator({"BTC":0.4, "LTC":0.2, "ETH":0.4})

    def test_instanciate_class_wrong_param2(self):
        with self.assertRaises(Exception):
            AllocationsTableGenerator([{"BTC":0.4, "LTC":0.2, "ETH":0.4}])
    
    def test_execute(self):
        df_we_want = pd.DataFrame(index=[1,2,3], columns=["BTC", "LTC", "ETH"])
        df_we_want["BTC"] = [float(0.4), float(0.5), float(0.5)]
        df_we_want["LTC"] = [float(0.2), float(0.5), float(0.2)]
        df_we_want["ETH"] = [float(0.4), float(0.5), float(0.3)]
        df_we_want = df_we_want.reindex(sorted(df_we_want.columns), axis=1)
        df_we_want = df_we_want.sort_index()
        index_we_want = [1600534800, 1600448400, 1600362000]
        index_we_want = [datetime.datetime.fromtimestamp(element) for element in index_we_want]
        df_we_want.index = index_we_want
        df_allocations_table_generator = AllocationsTableGenerator(self.list_allocations_results).execute()
        pd_testing.assert_frame_equal(df_we_want, df_allocations_table_generator)

