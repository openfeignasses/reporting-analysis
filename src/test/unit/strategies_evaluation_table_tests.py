import unittest
import datetime
import pandas as pd
import pandas.testing as pd_testing
from cryptobot_commons import InvestmentUniverse
from cryptobot_commons import Signal
from cryptobot_commons import StrategiesResults
from cryptobot_commons import CryptoCurrencyPair
from cryptobot_commons import CryptoCurrencyPairs
from strategy_evaluation_table_generator import StrategiesEvaluationTableGenerator


def generate_a_strategies_results(execution_id:int, currency_and_position:dict, date:int):
    strategies_results_raw = []
    for currency in list(currency_and_position.keys()):
        strategies_results_raw.append({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse[currency],
            "signal": Signal[currency_and_position[currency]],
            "add_date": datetime.datetime.fromtimestamp(date),
            "execution_id": execution_id
        })
    return StrategiesResults(strategies_results_raw)

def generate_a_crypto_pair(name:str, start_date:int, quotes:list):
    timestamp_list = [start_date + 3600 * i for i in range(len(quotes))]
    df = pd.DataFrame(index=timestamp_list)
    df["open"] = [0.0]*len(quotes)
    df["high"] = [0.0]*len(quotes)
    df["low"] = [0.0]*len(quotes)
    df["close"] = quotes
    df["vwap"] = [0.0]*len(quotes)
    df["volume"] = [0.0]*len(quotes)
    df["count"] = [0.0]*len(quotes)
    return CryptoCurrencyPair(name=name, data=df)


class StrategiesEvaluationTableGeneratorTestCase(unittest.TestCase):

    def setUp(self):
        list_currency_pairs = []
        list_currency_pairs.append(
            generate_a_crypto_pair(name='BTC_EUR', start_date=1588618800, quotes=[6,6,5,6,7,8,6,5,7,7,8,8,9,10,11,10,10,11,10,11,10,8,7])
        )
        list_currency_pairs.append(
            generate_a_crypto_pair(name='LTC_EUR', start_date=1588618800, quotes=[5,5,6,8,9,10,10,9,8,8,9,10,12,12,13,15,16,17,18,19,19,21,23])
        )
        list_currency_pairs.append(
            generate_a_crypto_pair(name='ETH_EUR', start_date=1588618800, quotes=[30,30,34,35,36,37,35,34,33,30,28,27,25,25,26,27,25,21,20,20,18,17])
        )
        self.currency_pairs = CryptoCurrencyPairs(list_currency_pairs)

    def test_instanciate_class(self):
        list_strategies_results = []
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=1, currency_and_position={"BTC_EUR":"LONG", "LTC_EUR":"LONG", "ETH_EUR":"LONG"}, date=1588619100)
        )
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=2, currency_and_position={"BTC_EUR":"SHORT", "LTC_EUR":"LONG", "ETH_EUR":"LONG"}, date=1588637100)
        )
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=3, currency_and_position={"BTC_EUR":"SHORT", "LTC_EUR":"NEUTRAL", "ETH_EUR":"LONG"}, date=1588655100)
        )
        StrategiesEvaluationTableGenerator(list_strategies_results, self.currency_pairs)

    def test_instanciate_class_wrong_param1(self):
        with self.assertRaises(Exception):
            StrategiesEvaluationTableGenerator({})

    def test_instanciate_class_wrong_param2(self):
        with self.assertRaises(Exception):
            StrategiesEvaluationTableGenerator(self.currency_pairs, [])

    def test_execute1(self):
        list_strategies_results = []
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=1, currency_and_position={"BTC_EUR":"LONG", "LTC_EUR":"LONG", "ETH_EUR":"LONG"}, date=1588619100)
        )
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=2, currency_and_position={"BTC_EUR":"SHORT", "LTC_EUR":"LONG", "ETH_EUR":"LONG"}, date=1588637100)
        )
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=3, currency_and_position={"BTC_EUR":"SHORT", "LTC_EUR":"NEUTRAL", "ETH_EUR":"LONG"}, date=1588655100)
        )
        df_we_want = pd.DataFrame(index=[1,2,3], columns=["BTC_EUR", "LTC_EUR", "ETH_EUR"])
        df_we_want["BTC_EUR"] = [True, str(None), str(None)]
        df_we_want["LTC_EUR"] = [True, str(None), str(None)]
        df_we_want["ETH_EUR"] = [True, str(None), str(None)]
        df_we_want = df_we_want.reindex(sorted(df_we_want.columns), axis=1)
        df_we_want = df_we_want.sort_index()
        index_we_want = [1588619100, 1588637100, 1588655100]
        index_we_want = [datetime.datetime.fromtimestamp(element) for element in index_we_want]
        df_we_want.index = index_we_want
        df_strategies_evaluation_table_generator = StrategiesEvaluationTableGenerator(list_strategies_results, self.currency_pairs).execute()
        pd_testing.assert_frame_equal(df_we_want, df_strategies_evaluation_table_generator)

    def test_execute2(self):
        list_strategies_results = []
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=1, currency_and_position={"BTC_EUR":"LONG", "LTC_EUR":"SHORT", "ETH_EUR":"LONG"}, date=1588619100)
        )
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=2, currency_and_position={"BTC_EUR":"LONG", "LTC_EUR":"SHORT", "ETH_EUR":"SHORT"}, date=1588637100)
        )
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=3, currency_and_position={"BTC_EUR":"SHORT", "LTC_EUR":"LONG", "ETH_EUR":"LONG"}, date=1588655100)
        )
        df_we_want = pd.DataFrame(index=[1,2,3], columns=["BTC_EUR", "LTC_EUR", "ETH_EUR"])
        df_we_want["BTC_EUR"] = [True, str(None), str(None)]
        df_we_want["LTC_EUR"] = [False, str(None), str(None)]
        df_we_want["ETH_EUR"] = [True, str(None), str(None)]
        df_we_want = df_we_want.reindex(sorted(df_we_want.columns), axis=1)
        df_we_want = df_we_want.sort_index()
        index_we_want = [1588619100, 1588637100, 1588655100]
        index_we_want = [datetime.datetime.fromtimestamp(element) for element in index_we_want]
        df_we_want.index = index_we_want
        df_strategies_evaluation_table_generator = StrategiesEvaluationTableGenerator(list_strategies_results, self.currency_pairs).execute()
        pd_testing.assert_frame_equal(df_we_want, df_strategies_evaluation_table_generator)

    def test_execute3(self):
        list_strategies_results = []
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=1, currency_and_position={"BTC_EUR":"LONG", "LTC_EUR":"SHORT", "ETH_EUR":"LONG"}, date=1588619100)
        )
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=2, currency_and_position={"BTC_EUR":"NEUTRAL", "LTC_EUR":"SHORT", "ETH_EUR":"SHORT"}, date=1588637100)
        )
        list_strategies_results.append(
            generate_a_strategies_results(execution_id=3, currency_and_position={"BTC_EUR":"SHORT", "LTC_EUR":"SHORT", "ETH_EUR":"LONG"}, date=1588655100)
        )
        df_we_want = pd.DataFrame(index=[1,2,3], columns=["BTC_EUR", "LTC_EUR", "ETH_EUR"])
        df_we_want["BTC_EUR"] = [True, str(None), str(None)]
        df_we_want["LTC_EUR"] = [False, str(None), str(None)]
        df_we_want["ETH_EUR"] = [True, str(None), str(None)]
        df_we_want = df_we_want.reindex(sorted(df_we_want.columns), axis=1)
        df_we_want = df_we_want.sort_index()
        index_we_want = [1588619100, 1588637100, 1588655100]
        index_we_want = [datetime.datetime.fromtimestamp(element) for element in index_we_want]
        df_we_want.index = index_we_want
        df_strategies_evaluation_table_generator = StrategiesEvaluationTableGenerator(list_strategies_results, self.currency_pairs).execute()
        pd_testing.assert_frame_equal(df_we_want, df_strategies_evaluation_table_generator)