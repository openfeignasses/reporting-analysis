from cryptobot_commons.retrievers import StrategiesResultsRetriever
from cryptobot_commons.retrievers.allocation_results_retriever import AllocationResultsRetriever
from cryptobot_commons.retrievers.crypto_currency_pairs_retriever import CryptoCurrencyPairsRetriever
from cryptobot_commons.book_of_confusion_matrixes import BookOfConfusionMatrixes

from allocations_table_generator import AllocationsTableGenerator
from strategies_table_generator import StrategiesTableGenerator
from percent_success_table_generator import PercentSuccessTableGenerator
from html_file_creator import HtmlFileCreator
from utils import Utils
from strategy_evaluation_table_generator import StrategiesEvaluationTableGenerator


MAX_VALUES_TO_DISPLAY = 20

strategies_results = StrategiesResultsRetriever("BY_STRATEGY", strategy_name="MACD").execute()
sorted_strategies_results = Utils.sort_strategies_by_execution_id(strategies_results)
currency_pairs = CryptoCurrencyPairsRetriever("ALL", None).execute()

def get_strategies_table_as_html(sorted_strategies_results, currency_pairs) -> str:
    
    strategy_table = StrategiesTableGenerator(sorted_strategies_results).execute()
    
    strategy_evaluation_table = StrategiesEvaluationTableGenerator(
        sorted_strategies_results, currency_pairs).execute()
    strategy_table_final = Utils.merge_strategy_tables(strategy_table, strategy_evaluation_table)
    return Utils.get_last_values_of_a_df(strategy_table_final, MAX_VALUES_TO_DISPLAY).to_html(classes="table", escape=False)

def get_percent_success_table_as_html(sorted_strategies_results, currency_pairs) ->str:
    return PercentSuccessTableGenerator(sorted_strategies_results, currency_pairs).execute().to_html(classes="table", escape=False)

def get_allocations_table_as_html() -> str:
    allocation_results = AllocationResultsRetriever("ALL").execute()
    sorted_allocation_results = Utils.sort_allocation_by_execution_id(allocation_results)
    allocation_table = AllocationsTableGenerator(sorted_allocation_results).execute()
    return Utils.get_last_values_of_a_df(allocation_table, MAX_VALUES_TO_DISPLAY).to_html(classes="table", escape=False)

def get_confusion_matrixes(sorted_strategies_results, currency_pairs) ->list:
    book = BookOfConfusionMatrixes(sorted_strategies_results[:-2], currency_pairs)
    book.write()
    book_of_confusion_matrixes = book.read()
    return book_of_confusion_matrixes


strategies_table_as_html = get_strategies_table_as_html(sorted_strategies_results, currency_pairs)
percent_success_table_as_html = get_percent_success_table_as_html(sorted_strategies_results, currency_pairs)
allocations_table_as_html = get_allocations_table_as_html()
book_of_confusion_matrixes = get_confusion_matrixes(sorted_strategies_results, currency_pairs)

HtmlFileCreator(strategies_table_as_html, 
                percent_success_table_as_html, 
                allocations_table_as_html, 
                book_of_confusion_matrixes
               ).create()


print("Report generated.")
