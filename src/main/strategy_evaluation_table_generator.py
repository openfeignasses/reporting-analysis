import pandas as pd
import numpy as np
from cryptobot_commons import StrategiesResults
from cryptobot_commons import CryptoCurrencyPairs
from cryptobot_commons import StrategyResultEvaluator


def unique(list_):
    x = np.array(list_) 
    return list(np.unique(x))


class StrategiesEvaluationTableGenerator:
    
    def __init__(self, list_strategies_results:list, currency_pairs:CryptoCurrencyPairs):
        self.list_strategies_results = list_strategies_results
        self.distinct_pairs = self._detect_all_distinct_crypto_pairs(self.list_strategies_results)
        self.distinct_execution_id = self._detect_all_distinct_execution_id(self.list_strategies_results)
        self.currency_pairs = currency_pairs
        self.check_initialized_attributes()

    def check_initialized_attributes(self):
        if type(self.list_strategies_results) != list:
            raise(Exception("The list_strategies_results must be of type list."))
        for result in self.list_strategies_results:
            if type(result) != StrategiesResults:
                raise(Exception("Each element of the list_strategies_results attribute must be of type StrategiesResults."))
        if type(self.currency_pairs) != CryptoCurrencyPairs:
            raise(Exception("The currency_pairs must be of type CryptoCurrencyPairs."))

    def execute(self):
        df_strategies_evaluation_table_generator = pd.DataFrame(index=self.distinct_execution_id, columns=self.distinct_pairs)
        df_strategies_evaluation_table_generator = df_strategies_evaluation_table_generator.reindex(sorted(df_strategies_evaluation_table_generator.columns), axis=1)
        df_strategies_evaluation_table_generator = df_strategies_evaluation_table_generator.sort_index()
        
        for strategies_results in self.list_strategies_results[:-2]:
            for result in strategies_results.results:
                target_pair = self._get_target_pair(self.currency_pairs, result.currency_pair.name)
                df_strategies_evaluation_table_generator.loc[result.execution_id, result.currency_pair.name] = \
                        StrategyResultEvaluator.evaluate_the_signal(strategy_result=result, crypto_currency_pair=target_pair)
        
        # The two last rows are not evaluated (no enough information)
        for strategies_results in self.list_strategies_results[-2:]:
            for result in strategies_results.results:
                df_strategies_evaluation_table_generator.loc[result.execution_id, result.currency_pair.name] = str(None)

        # Set the add date as index
        for strategies_results in self.list_strategies_results:
            df_strategies_evaluation_table_generator.rename(index={strategies_results.results[0].execution_id:strategies_results.results[0].add_date}, inplace=True)

        return df_strategies_evaluation_table_generator

    def _get_target_pair(self, currency_pairs:CryptoCurrencyPairs, target_pair_name:str):
        for pair in currency_pairs.pairs:
            if pair.name == target_pair_name:
                return pair
        raise(Exception('The target_pair_name is not in the currency_pairs.'))

    def _detect_all_distinct_crypto_pairs(self, list_strategies_results):
        pairs = []
        for strategies_results in self.list_strategies_results:
            for result in strategies_results.results:
                pairs.append(result.currency_pair.name)
        return unique(pairs)

    def _detect_all_distinct_execution_id(self, list_strategies_results):
        pairs = []
        for strategies_results in self.list_strategies_results:
            for result in strategies_results.results:
                pairs.append(result.execution_id)
        return unique(pairs)