import pandas as pd
import numpy as np 
from cryptobot_commons import StrategiesResults



def unique(list_): 
    x = np.array(list_) 
    return list(np.unique(x))


class StrategiesTableGenerator:
    
    def __init__(self, list_strategies_results:list):
        self.list_strategies_results = list_strategies_results
        self.distinct_pairs = self._detect_all_distinct_crypto_pairs(self.list_strategies_results)
        self.distinct_execution_id = self._detect_all_distinct_execution_id(self.list_strategies_results)
        self.check_initialized_attributes()

    def check_initialized_attributes(self):
        if type(self.list_strategies_results) != list:
            raise(Exception("The list_strategies_results must be of type list."))
        for result in self.list_strategies_results:
            if type(result) != StrategiesResults:
                raise(Exception("Each element of the list_strategies_results attribute must be of type StrategiesResults."))

    def execute(self):
        df_strategies_table_generator = pd.DataFrame(index=self.distinct_execution_id, columns=self.distinct_pairs)
        df_strategies_table_generator = df_strategies_table_generator.reindex(sorted(df_strategies_table_generator.columns), axis=1)
        df_strategies_table_generator = df_strategies_table_generator.sort_index()
        for strategies_results in self.list_strategies_results:
            for result in strategies_results.results:
                df_strategies_table_generator.loc[result.execution_id, result.currency_pair.name] = result.signal.name
            df_strategies_table_generator.rename(index={strategies_results.results[0].execution_id:strategies_results.results[0].add_date}, inplace=True)
        return df_strategies_table_generator

    def _detect_all_distinct_crypto_pairs(self, list_strategies_results):
        pairs = []
        for strategies_results in self.list_strategies_results:
            for result in strategies_results.results:
                pairs.append(result.currency_pair.name)
        return unique(pairs)

    def _detect_all_distinct_execution_id(self, list_strategies_results):
        pairs = []
        for strategies_results in self.list_strategies_results:
            for result in strategies_results.results:
                pairs.append(result.execution_id)
        return unique(pairs)