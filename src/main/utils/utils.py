import pandas as pd
from cryptobot_commons import AllocationResults
from cryptobot_commons import StrategiesResults


class Utils:

    INDICATOR_OK = '<div><span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:green"></span><div>'
    INDICATOR_NOT_OK = '<div><span class="glyphicon glyphicon-remove" aria-hidden="true" style="color:red"></span><div>'

    @staticmethod
    def sort_strategies_by_execution_id(strategies_results: StrategiesResults) -> list:
        if not strategies_results or len(strategies_results.results) == 0:
            raise Exception("Wrong input for sort_by_execution_id method")

        sorting_buffer = {}

        for strategy_result in strategies_results.results:
            sorting_buffer = Utils._add_to_sorting_buffer_if_not_exists(strategy_result.execution_id, sorting_buffer)
            if Utils._should_init_strategies_results(strategy_result.execution_id, sorting_buffer):
                sorting_buffer[strategy_result.execution_id] = StrategiesResults([strategy_result])
            else:
                sorting_buffer[strategy_result.execution_id].results.append(strategy_result)

        return Utils._transform_buffer_to_sequenced_list(sorting_buffer)

    @staticmethod
    def sort_allocation_by_execution_id(allocation_results: AllocationResults) -> list:
        if not allocation_results or len(allocation_results.results) == 0:
            raise Exception("Wrong input for sort_by_execution_id method")

        sorting_buffer = {}

        for allocation_results in allocation_results.results:
            sorting_buffer = Utils._add_to_sorting_buffer_if_not_exists(allocation_results.execution_id, sorting_buffer)
            if Utils._should_init_allocation_results(allocation_results.execution_id, sorting_buffer):
                sorting_buffer[allocation_results.execution_id] = AllocationResults([allocation_results])
            else:
                sorting_buffer[allocation_results.execution_id].results.append(allocation_results)

        return Utils._transform_buffer_to_sequenced_list(sorting_buffer)

    @staticmethod
    def _add_to_sorting_buffer_if_not_exists(execution_id: int, sorting_buffer: dict) -> dict:
        if not execution_id in sorting_buffer:
            sorting_buffer[execution_id] = None
        return sorting_buffer

    @staticmethod
    def _should_init_strategies_results(execution_id: int, sorting_buffer: dict) -> bool:
        return sorting_buffer[execution_id] is None

    @staticmethod
    def _should_init_allocation_results(execution_id: int, sorting_buffer: dict) -> bool:
        return sorting_buffer[execution_id] is None

    @staticmethod
    def _transform_buffer_to_sequenced_list(sorting_buffer) -> list:
        results_sorted = []
        for execution_id, results in sorted(sorting_buffer.items()):
            results_sorted.append(results)
        return results_sorted

    @staticmethod
    def merge_strategy_tables(strategy_table, strategy_evaluation_table):
        strategy_table_final = pd.DataFrame(index=strategy_table.index, columns=strategy_table.columns)
        last_index = strategy_table_final.tail(2).index
        for row in strategy_table_final.index:
            if row not in last_index:
                for col in strategy_table_final.columns:
                    strategy_table_final.loc[row, col] =  str(strategy_table.loc[row, col]) + ' ' + Utils.get_indicator_from_evaluation(strategy_evaluation_table.loc[row, col])
            else:
                for col in strategy_table_final.columns:
                    strategy_table_final.loc[row, col] =  str(strategy_table.loc[row, col])
        return strategy_table_final
    
    @staticmethod
    def get_last_values_of_a_df(data_frame_initial, nb_values:int):
        return data_frame_initial.tail(nb_values)

    @staticmethod
    def get_indicator_from_evaluation(evaluation_is_positive: bool):
        if evaluation_is_positive:
            return Utils.INDICATOR_OK
        else:
            return Utils.INDICATOR_NOT_OK
