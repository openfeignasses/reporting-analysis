from cryptobot_commons.retrievers import AllocationResultsRetriever
import datetime
import sys

last_allocation_result = AllocationResultsRetriever("LIMITED_ORDERED", limit=1).execute().results[0]
max_adddate_allocations = last_allocation_result.add_date

current_timestamp = datetime.datetime.now().timestamp()
ten_mins_ago = current_timestamp - 600

if max_adddate_allocations.timestamp() > ten_mins_ago:
    print(str(datetime.datetime.now()) + " | Should the program run ? Answer: yes")
    sys.exit(0)
else:
    print(str(datetime.datetime.now()) + " | Should the program run ? Answer: no")
    sys.exit(1)