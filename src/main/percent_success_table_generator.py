import pandas as pd
import numpy as np 
from cryptobot_commons import StrategiesResults
from cryptobot_commons import CryptoCurrencyPair
from cryptobot_commons import CryptoCurrencyPairs
from cryptobot_commons import StrategyResultEvaluator


def unique(list_):
    x = np.array(list_) 
    return list(np.unique(x))


class PercentSuccessTableGenerator:
    
    def __init__(self, list_strategies_results:list, currency_pairs:CryptoCurrencyPairs):
        self.list_strategies_results = list_strategies_results
        self.distinct_pairs = self._detect_all_distinct_crypto_pairs(self.list_strategies_results)
        self.distinct_strategies = self._detect_all_distinct_strategies(self.list_strategies_results)
        self.currency_pairs = currency_pairs
        self.check_initialized_attributes()

    def check_initialized_attributes(self):
        if type(self.list_strategies_results) != list:
            raise(Exception("The list_strategies_results must be of type list."))
        for result in self.list_strategies_results:
            if type(result) != StrategiesResults:
                raise(Exception("Each element of the list_strategies_results attribute must be of type StrategiesResults."))
        if type(self.currency_pairs) != CryptoCurrencyPairs:
            raise(Exception("The currency_pairs must be of type CryptoCurrencyPairs."))

    def execute(self):
        df_true = pd.DataFrame(data = np.zeros((len(self.distinct_strategies), len(self.distinct_pairs))), 
                                index=self.distinct_strategies, 
                                columns=self.distinct_pairs)
        df_false = pd.DataFrame(data = np.zeros((len(self.distinct_strategies), len(self.distinct_pairs))), 
                                index=self.distinct_strategies, 
                                columns=self.distinct_pairs)

        for strategies_results in self.list_strategies_results[:-2]:
            for result in strategies_results.results:
                target_pair = self._get_target_pair(self.currency_pairs, result.currency_pair.name)
                evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=result, crypto_currency_pair=target_pair)

                if evaluation == True:
                    df_true.loc[result.strategy_name, result.currency_pair.name] += 1
                elif evaluation == False:
                    df_false.loc[result.strategy_name, result.currency_pair.name] += 1
                else:
                    raise(Exception('The evaluation has failed.'))
        
        df_percent_sucess = df_true / (df_true+df_false)
        return df_percent_sucess

    def _detect_all_distinct_crypto_pairs(self, list_strategies_results):
        pairs = []
        for strategies_results in self.list_strategies_results:
            for result in strategies_results.results:
                pairs.append(result.currency_pair.name)
        return unique(pairs)

    def _detect_all_distinct_strategies(self, list_strategies_results):
        pairs = []
        for strategies_results in self.list_strategies_results:
            for result in strategies_results.results:
                pairs.append(result.strategy_name)
        return unique(pairs)

    def _get_target_pair(self, currency_pairs:list, target_pair_name:str):
        for pair in currency_pairs.pairs:
            if pair.name == target_pair_name:
                return pair
        raise(Exception('The target_pair_name is not in the currency_pairs.'))