import pandas as pd
import numpy as np 
from cryptobot_commons import AllocationResults



def unique(list_): 
    x = np.array(list_) 
    return list(np.unique(x))


class AllocationsTableGenerator:
    
    def __init__(self, list_allocations_results:list):
        self.list_allocations_results = list_allocations_results
        self.distinct_pairs = self._detect_all_distinct_crypto_pairs(self.list_allocations_results)
        self.distinct_execution_id = self._detect_all_distinct_execution_id(self.list_allocations_results)
        self.check_initialized_attributes()

    def check_initialized_attributes(self):
        if type(self.list_allocations_results) != list:
            raise(Exception("The list_allocations_results must be of type list."))
        for result in self.list_allocations_results:
            if type(result) != AllocationResults:
                raise(Exception("Each element of the list_allocations_results attribute must be of type AllocationResults."))

    def execute(self):
        df_allocations_table_generator = pd.DataFrame(index=self.distinct_execution_id, columns=self.distinct_pairs)
        df_allocations_table_generator = df_allocations_table_generator.reindex(sorted(df_allocations_table_generator.columns), axis=1)
        df_allocations_table_generator = df_allocations_table_generator.sort_index()
        for allocation_results in self.list_allocations_results:
            for result in allocation_results.results:
                df_allocations_table_generator.loc[result.execution_id, result.currency] = result.value_percent
            df_allocations_table_generator.rename(index={allocation_results.results[0].execution_id:allocation_results.results[0].add_date}, inplace=True)
        df_allocations_table_generator = df_allocations_table_generator.astype(float) 
        return df_allocations_table_generator

    def _detect_all_distinct_crypto_pairs(self, list_strategies_results):
        pairs = []
        for allocation_results in self.list_allocations_results:
            for result in allocation_results.results:
                pairs.append(result.currency)
        return unique(pairs)

    def _detect_all_distinct_execution_id(self, list_strategies_results):
        pairs = []
        for allocation_results in self.list_allocations_results:
            for result in allocation_results.results:
                pairs.append(result.execution_id)
        return unique(pairs)