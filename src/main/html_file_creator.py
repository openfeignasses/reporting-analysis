from datetime import datetime

class HtmlFileCreator:
    def __init__(self, strategies_table_as_html: str, percent_success_table_as_html:str, allocations_table_as_html: str, book_of_confusion_matrixes: list):
        self.strategies_table_as_html = strategies_table_as_html
        self.percent_success_table_as_html = percent_success_table_as_html
        self.allocations_table_as_html = allocations_table_as_html
        self.book_of_confusion_matrixes = book_of_confusion_matrixes
        self.page = ""
        self.boostrapA = '<table border="1" class="dataframe">'
        self.boostrapB = '<table class="table table-striped">'
        self.file = open('/workspace/src/outputs/reporting-analysis.html', 'w+')

    def create(self):
        self._wrap_tables_with_page_structure()
        self._write_page_to_file()

    def _wrap_tables_with_page_structure(self):
        date = str(datetime.now().strftime("%d/%m/%y - %H:%M"))
        self.page = '''
        <html>
            <head>
                <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
                <meta content="utf-8" http-equiv="encoding">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
                <style>body{ margin:0 100; background:whitesmoke; }</style>
            </head>
            <body>
                <center><h1>Cryptobot - Reporting</h1></center>
                <br />
                <center><p>(Date of generation: ''' + date + ''')</p></center>
                <br />
                <br />
                <h2>Strategy</h2>
                <br />
                ''' + self.strategies_table_as_html + '''
                <br />
                <br />
                <h2>Percent Success</h2>
                <br />
                ''' + self.percent_success_table_as_html + '''
                <h2>Confusion Matrixes</h2>
                <br />
                ''' + self.book_of_confusion_matrixes[0].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[0].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                ''' + self.book_of_confusion_matrixes[1].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[1].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                ''' + self.book_of_confusion_matrixes[2].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[2].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                ''' + self.book_of_confusion_matrixes[3].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[3].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                ''' + self.book_of_confusion_matrixes[4].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[4].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                ''' + self.book_of_confusion_matrixes[5].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[5].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                ''' + self.book_of_confusion_matrixes[6].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[6].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                ''' + self.book_of_confusion_matrixes[7].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[7].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                ''' + self.book_of_confusion_matrixes[8].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[8].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                ''' + self.book_of_confusion_matrixes[9].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[9].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                ''' + self.book_of_confusion_matrixes[10].crypto_currency_pair.name + '''
                <br />
                ''' + self.book_of_confusion_matrixes[10].matrix.to_html(classes="table", escape=False) + '''
                <br />
                <br />
                <h2>Allocation (%)</h2>
                <br />
                ''' + self.allocations_table_as_html + '''
            </body>
        </html>
        '''

    def _write_page_to_file(self):
        self.file.write(self.page)
        self.file.close()


