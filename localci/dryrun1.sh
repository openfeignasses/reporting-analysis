set -e # exit on fail

if [ ! -d "../database-migrator" ]; then
    echo "Directory ../database-migrator doesn't exist. Exiting ..."
    exit 1
fi
if [ ! -d "../commons-python" ]; then
    echo "Directory ../commons-python doesn't exist. Exiting ..."
    exit 1
fi
echo "Recompiling code ..."
cd ../commons-python
zip -r cryptobot-commons-latest.zip . -x venv/**\* -x .idea/**\* -x .git/**\*
cd ../reporting-analysis
docker build --build-arg SSH_PRIVATE_KEY="$(cat ../deploy_key)" \
             --build-arg=COMMONS_PYTHON_SHASUM="$(sha1sum ../commons-python/cryptobot-commons-latest.zip  | awk '{print $1}')" \
             -t reporting-analysis .
docker build -t database-migrator ../database-migrator
echo "Executing ..."
docker run \
    --rm \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator
docker run \
    --rm \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run clear-db
docker run \
    --rm \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run fixtures:reporting-analysis:dryrun1
docker run \
    --rm \
    -v $(pwd)/report:/workspace/src/outputs \
    -v $(pwd)/../commons-python:/commons-python \
    --link mysql:mysql \
    -e MYSQL_DATABASE=cryptobot \
    -e DATABASE_PORT="3306" \
    -e MYSQL_USER=admin \
    -e MYSQL_PASSWORD=frNH95eSiLYY \
    -e DATABASE_HOST=mysql \
    -e PYTHONPATH=/workspace/src/main/ \
    reporting-analysis \
    bash -c "pip install /commons-python/cryptobot-commons-latest.zip && cd src/main && python execute.py"
rm ../commons-python/cryptobot-commons-latest.zip