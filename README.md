# reporting-analysis

This Python module generates an HTML report after each execution. It is based on data of following modules :
- data-provider
- strategies
- allocations
## Requirements

- git
- docker

## Local development

*Commands must be run from the root of the module folder.*  
*The database must be up and running (see database-migrator module).*  
*The database-migrator folder must be present next to the current module's folder, like this:*  
```
.
├── database-migrator
├── reporting-analysis
├── ...
```

### Execute the module

Execution with this data set : `src/test/resources/data1.sql`:  
`./localci/dryrun1.sh`  
Execution with this data set : `src/test/resources/data2.sql`:  
`./localci/dryrun2.sh`

### Run tests

`./localci/test-unit-behaviour.sh`

Offline:  
It is possible to run tests offline, but you need to run the command while being online at least once.

```
Delivered with 💓 by


              /')                                                                           
            /' /' ____     O  ____     ,____     ____     ____     ____     ____     ____   
         -/'--' /'    )  /' /'    )   /'    )  /'    )  /'    )--/'    )--/'    )  /'    )--
        /'    /(___,/' /' /'    /'  /'    /' /'    /'  '---,    '---,   /(___,/'  '---,     
      /(_____(________(__(___,/(__/'    /(__(___,/(__(___,/   (___,/   (________(___,/      
    /'                      /'                                                              
  /'                /     /'                                                                
/'                 (___,/'                                                                  
```