max_adddate_allocations=$(mysql -uadmin -p$MYSQL_PASSWORD -h $DATABASE_HOST cryptobot < query_max_adddate_allocations.sql | tail -n 1)
current_timestamp=$(date +"%s")
ten_minutes_ago=$((current_timestamp - 600))

if [ $max_adddate_allocations -gt $ten_minutes_ago ]; then
    echo "$current_timestamp | Should the program run ? Answer: yes"
    exit 0
fi

echo "$current_timestamp | Should the program run ? Answer: no"
exit 1